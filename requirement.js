/* --------EXAMPLE--------- */
$(document).ready(function(){
  $("p").click(function(){
    $(this).hide();
  });
});

$("p").on({
  mouseenter: function(){
    $(this).css("background-color", "lightgray");
  },
  mouseleave: function(){
    $(this).css("background-color", "lightblue");
  },
  click: function(){
    $(this).css("background-color", "yellow");
  }
});

// query
$('#id')
$('.className')

// animations
$("button").click(function(){
  $("#p1").css("color", "red")
    .slideUp(2000)
    .slideDown(2000);
});



/* --------SET--------- */
//  text
$("#btn").click(function(){         // <-> text 
  $("#test").text("Hello world!");
});

//  html
$("#test").html("<b>Hello world!</b>");

// values
$("#test").val("Dolly Duck");

// attrib
$("#test").attr("href", "https://www.w3schools.com/jquery/");

// callback inside set
$("#btn").click(function(){
  $("#test").text(function(i, origText){
    return "Old text: " + origText + " New text: Hello world!(index: " + i + ")";
  });
});



/* --------ADD--------- */
// append, after, before
var txt = $("<p></p>").text("Yo.");
$("body").append(txt1, txt2, txt3);

// class
$("h1, h2, p").addClass("blue");



/* --------AJAX--------- */
// load
$("button").click(function(){
  $("#div1").load("demo_test.txt #p1", function(responseTxt, statusTxt, xhr){
    if(statusTxt == "success")
      alert("External content loaded successfully!");
    if(statusTxt == "error")
      alert("Error: " + xhr.status + ": " + xhr.statusText);
  });
});

// getJSON
$.getJSON( "example/test.json", function( data ) {
  var items = [];
  $.each( data, function( key, val ) {
    items.push( "<li id='" + key + "'>" + val + "</li>" );
  });
 
  $( "<ul/>", {
    "class": "my-new-list",
    html: items.join( "" )
  }).appendTo( "body" );
});


/*  
Yêu cầu
Sử dụng HTML CSS Javascript or Jquery để tạo giao diện sau
Làm trên các thiết bị có màn hình có chiều rộng lớn 1366 px và nhỏ hơn 414px, desktop và mobile
1. Màn hình login
Khi đăng nhập với tên: john, password: 1234 thì redirect sang trang dash board
Nếu đăng nhập sai tên hoặc password, sẽ hiển thị thông báo

2. Màn hình dashboard
Giả lập toàn bộ dữ liệu của bảng và biểu đồ
Hiển thị dữ liệu đã giả lập
Khi thêm mới thiết bị, nếu thiếu Name or ID thì validate
Khi thêm mới thành công, dữ liệu sẽ được thêm vào trong bảng

Chú ý: biểu đồ phải dùng thư viện JS để hiển thị, không phải dạng ảnh

3. Màn hình action logs
Giả lập dữ liệu logs
Hiển thị lên trên table
Có chức năng phân trang
"Có chức năng tìm kiếm theo tên thiết bị:

Khi người dùng nhập vào sau đó ấn search thì dữ liệu logs được hiển thị chỉ đối với thiết bị đó"

4. Trên thiết bị mobile
Sidebar menu mặc định ẩn, khi ấn vào humberger button thì hiển thị. Nếu người dùng ấn ra ngoài thì menu lại co lại như cũ
Chú ý màn hình có kích thước chiều rộng 414px
Không hiển thị bảng trong màn hình này
Chỉ cần làm màn hình dashboard cho mobile devices. Các màn hình khác không bắt buộc responsive.
*/