const component = {}

component.login = `
<div class="d-flex justify-content-center login-bg w-100 h-100"> 
    <div class="d-flex flex-column card">
        <h2 class="text-center">SOIOT SYSTEM</h2>
        <form id="formLogin"> 
            <div class="d-flex justify-content-center"> 
                <input type="text" placeholder="username" id="email">
            </div>
            <div class="d-flex justify-content-center">
                <input type="password"  placeholder="password" id="password">
            </div>
            <div class="d-flex justify-content-center align-items-center"> 
                <button class="btn-submit" type="submit"> LOGIN </button>
                <a class="text-center" id="btnMoveToRegister">or create new account</a>
            </div>
        </form>
    </div>
</div>
`

component.register = `
<div class="d-flex justify-content-center login-bg w-100 h-100"> 
    <div class="d-flex flex-column card">
        <h2 class="text-center">SOIOT SYSTEM</h2>
        <form id="formRegister"> 
            <div class="d-flex justify-content-center"> 
                <input type="text" placeholder="username" id="email">
            </div>
            <div class="d-flex justify-content-center">
                <input type="password"  placeholder="password" id="password">
            </div>
            <div class="d-flex justify-content-center">
                <input type="password"  placeholder="retype password" id="retypedPassword">
            </div>
            <div class="d-flex justify-content-center align-items-center"> 
                <button class="btn-submit" type="submit"> REGISTER </button>
                <a class="text-center" id="btnMoveToLogin">Already have account? Go to login</a>
            </div>
        </form>
    </div>
</div>
`

component.dashboard = `
<nav class="nav-header" onclick="showNav()">
    <ul class="nav-show">
        <li>
            <a class="d-flex align-items-center icon">
                <i class="bi bi-list"></i>
            </a>
        </li>
    </ul>
</nav>

<div class="menu" id="menu">
    <div class="d-flex flex-column">
        <nav class="nav-menu" id="nav-menu">
            <ul class="header">
                <li>
                    <a class="d-flex align-items-center">
                        <i class="bi bi-person"></i>
                        <span class="text-center" id="spanUser">Welcome</span>
                    </a>
                </li>
            </ul>

            <ul class="nav-top">
                <li>
                    <a class="d-flex align-items-center">
                        <i class="bi bi-building"></i>
                        <span class="text-center">Device Manager</span>
                    </a>
                </li>
            </ul>

            <ul class="nav-body" id="nav-body">
                <li id="dashboard"><a class="d-flex align-items-center">
                        <i class="bi bi-window"></i>
                        <span class="text-center">Dashboard</span>
                    </a>
                </li>
                <li id="logs"><a class="d-flex align-items-center">
                        <i class="bi bi-bootstrap-reboot"></i>
                        <span class="text-center">Logs</span>
                    </a>
                </li>
                <li id="settings"><a class="d-flex align-items-center">
                        <i class="bi bi-gear"></i>
                        <span class="text-center">Settings</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="main" onclick="removeNav()"> 
    <div style="overflow-x: auto; overflow-y: auto;">
        <table id="devicesTable" border="1">
        </table>
    </div>
    <div class="d-flex justify-content-center" id="pageNumberContainer">
    </div>
    <div class="d-flex chartContainer">
        <div class="d-flex flex-grow-1 align-items-center justify-content-center chart" id="chart">

        </div>
        <div class="d-flex flex-grow-1 flex-column card card-dashboard">
            <form class="" id="formDashboard"> 
                <div class="d-flex justify-content-center"> 
                    <input type="text" placeholder="name" id="deviceName">
                </div>
                <div class="d-flex justify-content-center">
                    <input type="text"  placeholder="IP" id="deviceIp">
                </div>
                <div class="d-flex justify-content-center align-items-center"> 
                    <button class="btn-submit" type="submit"> ADD DEVICE </button>
                </div>
            </form>
        </div>
    </div>
</div>

`

component.logs = `
<nav class="nav-header" onclick="showNav()">
    <ul class="nav-show">
        <li>
            <a class="d-flex align-items-center icon">
                <i class="bi bi-list"></i>
            </a>
        </li>
    </ul>
</nav>

<div class="menu" id="menu">
    <div class="d-flex flex-column">
        <nav class="nav-menu" id="nav-menu">
            <ul class="header">
                <li>
                    <a class="d-flex align-items-center">
                        <i class="bi bi-person"></i>
                        <span class="text-center" id="spanUser">Welcome</span>
                    </a>
                </li>
            </ul>

            <ul class="nav-top">
                <li>
                    <a class="d-flex align-items-center">
                        <i class="bi bi-building"></i>
                        <span class="text-center">Device Manager</span>
                    </a>
                </li>
            </ul>

            <ul class="nav-body" id="nav-body">
                <li id="dashboard"><a class="d-flex align-items-center">
                        <i class="bi bi-window"></i>
                        <span class="text-center">Dashboard</span>
                    </a>
                </li>
                <li id="logs"><a class="d-flex align-items-center">
                        <i class="bi bi-bootstrap-reboot"></i>
                        <span class="text-center">Logs</span>
                    </a>
                </li>
                <li id="settings"><a class="d-flex align-items-center">
                        <i class="bi bi-gear"></i>
                        <span class="text-center">Settings</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="main" onclick="removeNav()"> 
    <div class="d-flex space-between align-items-center search-bar"> 
        <h2> Action Logs </h2>
        <form id="formSearch" onsubmit="return searchByName(event)"> 
            <input type="text"  placeholder="name" id="searchName">
            <button class="btn-submit" type="submit"> Search </button>
        </form>
    </div>
    <div style="overflow-x: auto;">
        <table id="devicesTable" border="1">
        </table>
    </div>

    <div class="d-flex justify-content-center" id="pageNumberContainer">
    </div>
</div>

`