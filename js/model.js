const model = {}

let authedUser = ""
let navigations = []
let activeNav = ""
let currentScreen = ""
let pageNumber = 0
let totalPageNumber = 0

let ROWS_PER_PAGE = 1
const API_URLS = ["https://623014bdf113bfceed47c485.mockapi.io/api/devices", 
            "https://623014bdf113bfceed47c485.mockapi.io/api/logs"]

let devices = []
let logs = []
let chartDevices = []
let chart

model.changeAuthedUser = (user) => {
    authedUser = user
}

model.changePageNumber = (number) => {
    pageNumber = number
}

model.changeTotalPageNumber = () => {
    if (currentScreen === "dashboard") {
        ROWS_PER_PAGE = 3
        totalPageNumber = Math.ceil(devices.length / ROWS_PER_PAGE)
    }
    else if (currentScreen === "logs") {
        ROWS_PER_PAGE = 10
        totalPageNumber = Math.ceil(logs.length / ROWS_PER_PAGE)
    }
}

model.changeCurrentScreen = (screenName) => {
    currentScreen = screenName
}

model.subscribeListDevices = (type, newDevice) => {
    if (type === "added") {
        const promiseArray = API_URLS.map(url => fetch(url))
        Promise.all(promiseArray).then(responses =>
            Promise.all(responses.map(res => res.json()))
        ).then(data => {
            // devices data
            let date, formattedDate, day, month, year
            data[0].forEach(element => {
                date = new Date(element.created_at)
                day = date.getDate()
                month = date.getMonth()
                year = date.getFullYear()

                formattedDate = `${day}/${month}/${year}`

                devices.push({
                    "Device": element.name,
                    "MAC address": element.mac_address,
                    "IP": element.ip,
                    "Created Date": formattedDate,
                    "Power consumption": element.power_consumption,
                })
            });

            // devices data for d3 chart
            data[0].forEach(element => {
                date = new Date(element.date)
                day = date.getDate()
                month = date.getMonth()
                year = date.getFullYear()

                formattedDate = `${day}/${month}/${year}`

                chartDevices.push({
                    "name": element.name,
                    "value": element.power_consumption,
                })
            });

            chart = DonutChart(chartDevices, {
                name: d => d.name,
                value: d => d.value,
                width: 400,
                height:300  
            })
            setTimeout(() => {
                model.notifyChartChanges()
            }, 3000);

            // logs data
            data[1].forEach(element => {
                date = new Date(element.date)
                day = date.getDate()
                month = date.getMonth()
                year = date.getFullYear()

                formattedDate = `${day}/${month}/${year}`

                // logs.push({...element, date: formattedDate})
                logs.push({
                    "Device ID": element.id,
                    "Name": element.name,
                    "Action": element.action ? element.action : "false",
                    "Date": formattedDate,
                })
            });
            model.notifyDevicesChanges()
        })
    } else if (type === "modified") {
        if (newDevice) {
            date = new Date()
            day = date.getDate()
            month = date.getMonth()
            year = date.getFullYear()
            formattedDate = `${day}/${month}/${year}`
    
            devices.push({
                "Device": newDevice.deviceName,
                "MAC address": generateRandomMacAddress(),
                "IP": newDevice.deviceIp,
                "Created Date": formattedDate,
                "Power consumption": Math.floor((Math.random() * 400) + 200), // 200 -> 600Kwh
            })
        }
        model.notifyChartChanges()
        model.notifyDevicesChanges()
    }
}

model.changeActiveNav = (navId) => {
    if (navId !== activeNav) {
        activeNav = navId
    }
    model.notifyActiveNavChanges()
}

model.notifyDevicesChanges = () => {
    view.onDevicesChanges(devices)
}

model.notifyActiveNavChanges = () => {
    view.onActiveNavChanges()
}

model.notifyChartChanges = () => {
    view.onChartChanges()
}
