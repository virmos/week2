const controller = {}

controller.createDevice = (payload) => {
    if (payload.deviceName.length === 0 || payload.deviceIp.length === 0) {
        throw new Error(`Device name or IP address can't be empty`)
    }

    model.subscribeListDevices("modified", payload)
}

controller.updatePageNumber = (number) => {
    model.changePageNumber(number);
}

controller.updateActiveNav = (navId) => {
    model.changeActiveNav(navId)
}

controller.register = (payload) => {
    // TODO
    return true
}

controller.login = (payload) => {
    // TODO: remove hardcoded mail and password
    if (payload.email.length === 0 || payload.password.length === 0) {
        throw new Error(`Email or password can't be empty`)
    }
    let loginResult = false
    if (payload.email === "john" && payload.password === "1234")
        loginResult = true
    if (!loginResult) {
        throw new Error("User is not verified!")
    }

    model.changeAuthedUser(payload.email)
    model.changeCurrentScreen("dashboard")
    return true;
}