const view = {}

view.setActiveScreen = (screenName) => {
    document.getElementById('app').innerHTML = component[screenName]
    
    switch(screenName) {
        case 'login':
            const btnMoveToRegister = document.getElementById("btnMoveToRegister")
            btnMoveToRegister.addEventListener("click", function(event) {
                event.preventDefault()
                view.setActiveScreen('register')
            })
            const formLogin = document.getElementById("formLogin")
            formLogin.addEventListener("submit", async function(event) {
                event.preventDefault()
                const email = formLogin.email.value
                const password = formLogin.password.value
                const payload = {email: email, password: password}
                try {
                    const success = controller.login(payload)
                    if (success) {
                        view.setActiveScreen('dashboard')
                    }
                } catch(err) {
                    alert(err.message)
                }
                model.subscribeListDevices("added")                 // GET DATA ONLY ONCE
            })
            break;

        case 'register':
            document.getElementById("btnMoveToLogin").addEventListener("click", function() {
                view.setActiveScreen('login')
            })
            const formRegister = document.getElementById("formRegister")
            formRegister.addEventListener("submit", async function(event) {
                event.preventDefault()
                const userInput = {
                    email: formRegister.email.value,
                    displayName: formRegister.name.value,
                    password: formRegister.password.value,
                    retypePassword: formRegister.retypePassword.value,
                }
                try {
                    const result = controller.register(userInput)
                    if (result) alert("Register successfully! Check your inbox!")
                } catch(err) {
                    alert(err.message)
                }
            })
            break;

        case 'dashboard':
            model.changeActiveNav("dashboard")
            model.changeCurrentScreen("dashboard")
            model.subscribeListDevices("modified")                 // GET DATA ONLY ONCE

            document.getElementById("spanUser").textContent = `Welcome ${authedUser}`

            const formDashboard = document.getElementById("formDashboard")
            formDashboard.addEventListener("submit", async function(event) {
                event.preventDefault()
                const deviceInput = {
                    deviceName: formDashboard.deviceName.value,
                    deviceIp: formDashboard.deviceIp.value,
                }
                controller.createDevice(deviceInput)
            })

            break;

        case 'logs':
            document.getElementById("spanUser").textContent = `Welcome ${authedUser}`

            model.changeActiveNav("logs")
            model.changeCurrentScreen("logs")
            model.subscribeListDevices("modified")                 // GET DATA ONLY ONCE
            break;
    }
}

view.onDevicesChanges = () => {
    const devicesTable = document.getElementById("devicesTable")
    const table = currentScreen === "logs" ? logs : devices
    model.changeTotalPageNumber()
    buildHtmlTable(table, devicesTable)

    const pageNumberContainer = document.getElementById("pageNumberContainer")
    pageNumberContainer.innerHTML = ""
    for (let i = 1; i <= totalPageNumber; i++) {
        const btnHtml = document.createElement("button")
        btnHtml.classList.add("btn-rounded")
        btnHtml.textContent = i

        btnHtml.addEventListener("click", async function(event) {
            event.preventDefault()
            controller.updatePageNumber(parseInt(this.textContent) - 1)
            buildHtmlTable(table, devicesTable, pageNumber)
        })
        pageNumberContainer.append(btnHtml)
    }

    const listNav = document.querySelectorAll("#nav-body > li")
    listNav.forEach(function(navLi) {
        navLi.addEventListener("click", () => {
            controller.updateActiveNav(navLi.id); // name === id in this case
        })
    })
    controller.updateActiveNav(currentScreen);
}

view.onActiveNavChanges = () => {
    const listNav = document.querySelectorAll("#nav-body > li")
    listNav.forEach(function(navLi) {
        if (navLi.id !== activeNav) {
            navLi.classList.remove("active")
        } else {
            navLi.classList.add("active")
        }
    })
    view.onActiveNavUpdate()
}

view.onActiveNavUpdate = () => {
    if (currentScreen !== activeNav) {
        if (activeNav === "settings")
            alert("This site is under maintainance!")
        else {
            model.changeCurrentScreen(activeNav)
            view.setActiveScreen(activeNav)
        }
    }
}

view.onChartChanges = () => {
    const chartHTML = document.getElementById("chart")
    if (chartHTML) {
        chartHTML.innerHTML = ""
        chartHTML.append(chart)
    }
}
